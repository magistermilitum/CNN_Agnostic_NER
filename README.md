# Named Entity Recognition using Convolutional Neural Network

## Overview
The small project using Convolutional Neural Network (CNN) to solve the Named Entity Recognition (NER) problem.

## Requirements
* Python 3.6 or higher
* [Keras](https://keras.io/): Using Tensorflow backend
* [word2vec](https://pypi.org/project/gensim/): Python wrapper for [Tensorflow's word2vec algorithm](https://www.tensorflow.org/tutorials/word2vec)  
*For more requirement packages, see the file [requirements.txt](requirements.txt) or using python pip*
```
pip install -r requirements.txt
```

## Dataset Information
The data was used to evaluate the method is
[CONLL the CONLL-2003 NER Shared Task dataset (Tjong Kim Sang and De Meulder, 2003)](http://www.aclweb.org/anthology/W03-0419). 

## License
This project is licensed under the MIT License.
